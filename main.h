/**
 * @file
 * @brief PSR Semestral Work - Motor Control System
 *
 * This application consists of a server and a client component for controlling
 * and monitoring a motor's position. The server receives position updates from
 * the client, adjusts its motor position accordingly, and serves a web page with
 * real-time motor information. The client reads its motor position, sends updates
 * to the server, and allows manual control via keyboard input.
 *
 * Authors: Sukdol Jakub, Marousek Stepan
 * Created on: Dec 3, 2023
 */

/**
 * @brief Main entry point for the PSR Semestral Work application.
 *
 * Initializes the motor, semaphore, and sets up tasks based on the mode
 * (server or client) and parameters.
 *
 * @param ip_addr IP address of the server for the client mode. Use an empty string for the server mode.
 * @param port Port number for communication.
 */
void start(char *ip_addr, int port);
