/*
 * udp.c
 *
 *  Created on: Dec 13, 2023
 *      Author: Sukdol, Marousek
 */

#define UDP

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sysLib.h>
#include <semLib.h>
#include <taskLib.h>

#include <inetLib.h>
#include <sockLib.h>

#include "udp.h"



void sender(char* ip_addr, int portus, volatile int *position) {
	
	int last_known_position;
	
	struct sockaddr_in my_addr, srv_addr;
	int sockd;
	
	int count;
	int addrlen;
	
	/* Create a UDP socket */
	sockd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockd == -1) {
		perror("Socket creation error");
		exit(1);
	}

	/* Configure client address */
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = INADDR_ANY;
	my_addr.sin_port = 0;
	
	bind(sockd, (struct sockaddr*)&my_addr, sizeof(my_addr));

	/* Set server address */
	srv_addr.sin_family = AF_INET;
	inet_aton(ip_addr, &srv_addr.sin_addr);
	srv_addr.sin_port = htons(portus);
	
	last_known_position = -1;
	while (1) {
		if (last_known_position != *position) {
			// position changed or first iteration -> send current position
			sendto(sockd, position, sizeof(int), 0, (struct sockaddr*)&srv_addr, sizeof(srv_addr));
			printf("Pos %d sent\n", *position);
		}
		
		last_known_position = *position;
		
		// delay ...
		taskDelay(UDP_PAUSE);
	}
}


void receiver(int portus, volatile int *actual_position, volatile int *desired_position) {
	int sockd;
	struct sockaddr_in my_name, cli_name;
		 
	int status;
	int addrlen;
	int last_desired_position;
	/* Create a UDP socket */
	sockd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockd == -1)
	{
		perror("Socket creation error");
		exit(1);
	}
	 
	/* Configure server address */
	my_name.sin_family = AF_INET;
	my_name.sin_addr.s_addr = INADDR_ANY;
	my_name.sin_port = htons(portus);
	 
	status = bind(sockd, (struct sockaddr*)&my_name, sizeof(my_name));
	 
	addrlen = sizeof(cli_name);
	
	last_desired_position = -1;
	while (1) {
		status = recvfrom(sockd, desired_position, sizeof(int), 0, (struct sockaddr*)&cli_name, &addrlen);
		printf("Pos %d recv\n", *desired_position / 10);
		if ((last_desired_position != *desired_position) && 
			    (*desired_position != *actual_position)) {
			semGive(changeMotorSEM);
		}
		
		last_desired_position = *desired_position;
	}
}
