/**
 * @file motor_driver.h
 *
 *  Created on: Dec 3, 2023
 *      Author: Sukdol Jakub, Marousek Stepan
 *
 *  	Header file for handling interrupts from motor. Motor ISR increments extern semaphore semMotor.
 *  	Than handling function that was waiting for the semaphore is unblocked
 *  	
 *  	
 */


#ifndef MOTOR_DRIVER_H
#define MOTOR_DRIVER_H

#include "vxWorks.h"

// Semaphore that is incremented by motor ISR 
#ifdef MOTOR
SEM_ID motorIsrSem;
#else
extern SEM_ID motorIsrSem;
#endif

// Motor driver data
struct psrMotor {
    VIRT_ADDR fpgaRegs;
    VIRT_ADDR gpioRegs;
    UINT32 gpioIrqBit;
};

/**
 * @brief Probe function for the motor driver.
 * @param pInst Motor driver instance.
 * @return OK if the motor is recognized, ERROR otherwise.
 */
LOCAL STATUS motorProbe(VXB_DEV_ID pInst);

/**
 * @brief Interrupt service routine for the motor.
 * @param pMotor Motor structure.
 */
void motorISR(struct psrMotor *pMotor);

/**
 * @brief Attach function for the motor driver.
 * @param pInst Motor driver instance.
 * @return OK if the motor is attached successfully, ERROR otherwise.
 */
LOCAL STATUS motorAttach(VXB_DEV_ID pInst);

/**
 * @brief Shutdown function for the motor driver.
 * @param pDev Motor driver device.
 * @return OK if the motor is shut down successfully, ERROR otherwise.
 */
LOCAL STATUS motorShutdown(VXB_DEV_ID pDev);

/**
 * @brief Detach function for the motor driver.
 * @param pDev Motor driver device.
 * @return OK if the motor is detached successfully, ERROR otherwise.
 */
LOCAL STATUS motorDetach(VXB_DEV_ID pDev);

/**
 * @brief Initialize the motor device.
 * @return Pointer to the initialized motor structure, or NULL on failure.
 */
struct psrMotor *motorInit();


/**
 * @brief Cleanup and release resources for the motor device.
 */
void motorEnd();


#endif
