/**
 * @file main.c
 * @brief Main file for PSR semester project.
 *
 * This file contains the main functions and logic for the PSR semester project.
 * It initializes the motor, sets up tasks for server and client, and provides
 * control options for both.
 *
 * @date Dec 3, 2023
 * @authors Sukdol Jakub, Marousek Stepan
 */

#include <stdio.h>
#include <semLib.h>
#include <taskLib.h>
#include <string.h>
#include <sysLib.h>
#include <subsys/timer/vxbTimerLib.h>

#include "motor_driver.h"
#include "registers.h"
#include "udp.h"
#include "web_server.h"

#define PWM_PERIOD_20KHZ 5000

#define IRQ_PER_REV 512
#define IRQ_HALF_REV 256

#define MIN_DUTY ((int)(PWM_PERIOD_20KHZ / IRQ_HALF_REV)) 

#define CW 0 // clockwise
#define CCW 1 // counter clockwise
#define UNDEF -1

/** Desired motor position */
volatile int desired_pos = 0;
/** Actual motor position */
volatile int actual_pos = 0;

int pwm_percent = 0;

SEM_ID monitorSEM;

/**
 * @brief Main function for monitoring task.
 *
 * This function updates arrays for the web server with the desired, actual, and PWM
 * values for motor positions.
 */
void monitor(void)
{
	while (1) {
		semTake(arr_mtx, WAIT_FOREVER);
		desired_arr[last] = desired_pos;
		actual_arr[last] = actual_pos;
		pwm_arr[last] = pwm_percent * 5;
		
		last = (last + 1);
		if (last == WWW_ARRAY_SIZE) last = 0;
		if (last == first) {
			first = (first + 1);
			if (first == WWW_ARRAY_SIZE) first = 0;
		}
		semGive(arr_mtx);
		taskDelay(5);
	}
}


/**
 * @brief Update the motor position with the given position update.
 *
 * This function updates the motor position based on the given position update.
 * It ensures that the motor position remains within the valid range (0 to IRQ_PER_REV-1)
 * by handling wraparound conditions.
 *
 * @param pos_update Position update value to add to the current motor position.
 * @param position Pointer to the motor position variable to be updated.
 */
void update_position(int pos_update, volatile int *position)
{
	*position += pos_update;
	if (*position < 0) *position = IRQ_PER_REV + *position;
	if (*position >= IRQ_PER_REV) *position = (*position - IRQ_PER_REV);
}

/**
 * @brief P regulator (Proportional regulator) for motor position.
 *
 * This function implements a Proportional (P) regulator to control the motor position.
 * It calculates the direction and duty cycle of the PWM signal based on the difference
 * between the desired and actual positions. The regulator adjusts the motor position to
 * match the desired position over time.
 *
 * @param desired Desired position of the motor.
 * @param actual Actual position of the motor.
 * @param pMotor Pointer to the motor structure representing the server's motor.
 */
void regulate_motor(int desired, int actual, struct psrMotor *pMotor) {
	unsigned long dir ;
	unsigned duty ;
	int delta;
	
	if ((desired - actual + IRQ_PER_REV) % IRQ_PER_REV < IRQ_HALF_REV) {
	    // clockwise
		dir =  0x40000000;
		delta = ((desired - actual + IRQ_PER_REV) % IRQ_HALF_REV);
		
	} else {
	    // counter-clockwise
		dir = 0x80000000;
		delta = ((actual - desired + IRQ_PER_REV) % IRQ_HALF_REV);
	}

	duty = delta * 50;
	duty = duty > PWM_PERIOD_20KHZ ? PWM_PERIOD_20KHZ : duty;
	
	pwm_percent = duty * 100 / PWM_PERIOD_20KHZ;
	pwm_percent *= (dir == 0x80000000) ? -1 :1;
	
	FPGA_PWM_DUTY(pMotor) = duty | dir;	
}

/**
 * @brief Function that sets the server's motor to the desired position using a P(ID) regulator.
 *
 * This function continuously regulates the server's motor position based on the desired
 * position using a proportional-integral-derivative (P(ID)) regulator. It calculates the
 * direction and duty cycle of the PWM signal to control the motor movement. The regulator
 * adjusts the motor position to match the desired position over time.
 *
 * @param pMotor Pointer to the motor structure representing the server's motor.
 */
void motor_setter(struct psrMotor *pMotor)
{
	printf("Motor setter started\n");
	
	FPGA_CR(pMotor) = 0 | FPGA_CR_PWM_EN;
	FPGA_PWM_PERIOD(pMotor) = PWM_PERIOD_20KHZ;
	
	while(1) {

		regulate_motor(desired_pos, actual_pos, pMotor);
		
		taskDelay(1);
	}
}

/**
 * @brief Function that reads the position of the client's motor.
 *
 * This function continuously reads the position of the client's motor using
 * interrupts and updates the actual position. It utilizes the quadrature encoder
 * signals to determine the motor direction (clockwise or counter-clockwise) and
 * updates the actual position accordingly. The information is then used to control
 * the motor and synchronize with the server.
 *
 * @param pMotor Pointer to the motor structure representing the client's motor.
 */
void motor_reader(struct psrMotor *pMotor) {
	printf("Motor reader started\n");
	
	int q1, q2;
	int val = FPGA_SR(pMotor);
	_Bool a = (_Bool)(0x100 & val);
	_Bool b = (_Bool)(0x200 & val);
	q1 = (a << 1) + b;
	int pos_update = 0;
	while (1) {
		semTake(motorIsrSem, WAIT_FOREVER);
		val = FPGA_SR(pMotor);
		a = (_Bool)(0x100 & val);
		b = (_Bool)(0x200 & val);
	
		q2 = (a << 1) + b;
		
		_Bool clockwise = (
				((q1 == 1) && (q2 == 0)) ||
				((q1 == 0) && (q2 == 2)) ||
				((q1 == 2) && (q2 == 3)) ||
				((q1 == 3) && (q2 == 1)) );
		
		_Bool counter = (
				((q1 == 3) && (q2 == 2)) ||
				((q1 == 2) && (q2 == 0)) ||
				((q1 == 0) && (q2 == 1)) ||
				((q1 == 1) && (q2 == 3)) );
		
		q1 = q2;
		
		int direction = clockwise ? CW : counter ? CCW : UNDEF;
			
		if (clockwise) {
			pos_update = 1;
		} else if (counter) {
			pos_update = -1;
			
		} else {
			continue;
		}
		
		update_position(pos_update, &actual_pos);
		semGive(changeMotorSEM);
	}
}

/**
 * @brief General function for controlling both server and client.
 *
 * This function reads keyboard input and provides basic control options for both
 * the server and client applications. Press 'q' to quit, '+' to increment the desired
 * motor position, and '-' to decrement the desired motor position.
 */
void keyboard_input()
{
	char c;
	while (1) {
		scanf("%c", &c);
		if (c == 'q') {
			break;
		} else if (c == '+') {
			update_position(300, &desired_pos);
			printf("desired = %d, actual = %d\n", desired_pos / 10, actual_pos / 10);
			semGive(changeMotorSEM);
		} else if (c == '-') {
			update_position(-300, &desired_pos);
			printf("desired = %d, actual = %d\n", desired_pos / 10, actual_pos / 10);
			semGive(changeMotorSEM);
		}
		
	}
}

/**
 * @brief Main function for the client application.
 *
 * This function initializes tasks for reading the position of the client's motor
 * and sending it via Ethernet to the server. It also listens for keyboard input
 * to manually control the motor.
 *
 * @param ip IP address of the server.
 * @param port Port number for communication.
 * @param pMotor Pointer to the motor structure.
 */
void client(char *ip, int port, struct psrMotor *pMotor)
{
	printf("Client started\n");
	TASK_ID motorReader = taskSpawn("tMotorReader", 0, 0, 4096, (FUNCPTR) motor_reader, pMotor, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	TASK_ID task_sender = taskSpawn("UDP_sender",   0, 0, 4096, (FUNCPTR)sender, ip, port, &actual_pos, 0, 0, 0, 0, 0, 0, 0);

	if (motorReader == ERROR || task_sender == ERROR) {
		perror("Task spawn\n");
		return ;
	}
	keyboard_input();
	taskDelete(motorReader);
}


/**
 * @brief Main function for the server application.
 *
 * This function initializes necessary resources, spawns tasks for motor control,
 * UDP communication, monitoring, and the web server. It also listens for keyboard
 * input to control the motor manually.
 *
 * @param port Port number for communication.
 * @param pMotor Pointer to the motor structure.
 */
void server(int port, struct psrMotor *pMotor)
{
	printf("Server started\n");
	arr_mtx = semMCreate(SEM_Q_FIFO);
	if (arr_mtx == NULL) {
		perror("semMCreate\n");
		return;
	}
	
	TASK_ID motorSetter   = taskSpawn("tMotorSetter", 100, 0, 4096, (FUNCPTR) motor_setter, pMotor, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	TASK_ID motorReader   = taskSpawn("tMotorReader", 100, 0, 4096, (FUNCPTR) motor_reader, pMotor, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	TASK_ID task_receiver = taskSpawn("UDP_receiver", 125, 0, 4096, (FUNCPTR)receiver, port, &actual_pos, &desired_pos, 0, 0, 0, 0, 0, 0, 0);
	TASK_ID task_monitor = taskSpawn("tMonitor", 200, 0, 10000, (FUNCPTR) monitor, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	TASK_ID server = taskSpawn("tServer", 120, 0, 50000, (FUNCPTR) www, &actual_pos, &desired_pos, &pwm_percent, 0, 0, 0, 0, 0, 0, 0);
	
	if (motorSetter == ERROR || motorReader == ERROR || task_receiver == ERROR) {
		if (motorSetter != ERROR) taskDelete(motorSetter);
		if (motorReader != ERROR) taskDelete(motorReader);
		if (task_receiver != ERROR) taskDelete(task_receiver);
		if (server != ERROR) taskDelete(server);
		if (task_monitor != ERROR) taskDelete(task_monitor);
		perror("Task spawn\n");
		return ;
	}
	keyboard_input();
	taskDelete(motorSetter);
	taskDelete(motorReader);
	taskDelete(task_receiver);
	taskDelete(task_monitor);
	taskDelete(server);
}

void start(char *ip_addr, int port)
{
	sysClkRateSet(1000);
	
	struct psrMotor *pMotor = motorInit();
	monitorSEM = semCCreate(SEM_Q_FIFO, 0);
	changeMotorSEM = semCCreate(SEM_Q_FIFO, 0);
	if (monitorSEM == NULL || changeMotorSEM == NULL) {
			fprintf(stderr, "semCreate monitorSEM failed\n");
			motorEnd();
			return ;
		}
	if (pMotor == NULL) {
		fprintf(stderr, "Motor initialization failed!\n");
		return ;
	}
	
	if (strcmp(ip_addr, "") == 0) {
		server(port, pMotor);
	} else {
		client(ip_addr, port, pMotor);
	}
	
	motorEnd();
	
	printf("Motor controller has ended\nGoodbye!\n");
}




