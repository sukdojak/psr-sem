/**
 * @file udp.h
 * @brief UDP communication functions for motor control.
 *
 * This file contains functions for sending and receiving motor positions
 * using UDP communication.
 *
 * @date Dec 13, 2023
 * @author Sukdol, Marousek
 */

#ifndef UDP_H
#define UDP_H

#define UDP_PAUSE 2

#ifdef UDP
SEM_ID changeMotorSEM;
#else
extern SEM_ID changeMotorSEM;
#endif

/**
 * @brief Send motor position to a specified IP address and port.
 *
 * This function continuously sends the motor position to the specified
 * IP address and port using UDP communication.
 *
 * @param ip_addr IP address of the receiver.
 * @param portus UDP port number.
 * @param position Pointer to the volatile variable storing the motor position.
 */
void sender(char* ip_addr, int portus, volatile int *position);

/**
 * @brief Receive desired motor position from a specified port.
 *
 * This function continuously receives the desired motor position from the
 * specified UDP port and triggers a semaphore when the desired position changes.
 *
 * @param portus UDP port number.
 * @param actual_position Pointer to the volatile variable storing the actual motor position.
 * @param desired_position Pointer to the volatile variable storing the desired motor position.
 */
void receiver(int portus, volatile int *actual_position, volatile int *desired_position);

#endif
