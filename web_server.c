#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <sockLib.h>
#include <string.h>
#include <stdlib.h>
#include <taskLib.h>

#define SERVER

#include "web_server.h"

#define SERVER_PORT     80 /* Port 80 is reserved for HTTP protocol */
#define SERVER_MAX_CONNECTIONS  20

char *str_from_array(int *arr) {
	
	char *string = (char*)malloc((WWW_ARRAY_SIZE + 1) * 12);

	string[0] = '\0';
	
	int idx = 0;

	// Loop through the array and concatenate index and value with commas
	for (size_t i = first; i != last; i++) {
		if (i == WWW_ARRAY_SIZE) {
			i = 0;
		}
		
		sprintf(string + strlen(string), "%d,%d \0", idx, -1*arr[i]/5);
		// Add a space after each element (except the last one)
		idx++;
	}
	
	return string;
}

void server_task(int newFd)
{
	FILE *f = fdopen(newFd, "r+");
	setvbuf(f, NULL, _IONBF, 0); /* Disable buffering to work around VxWorks bug */
	char filename[100], http_ver[10];
	int ret = fscanf(f, "GET %99s %9s\n", filename, http_ver);
	if (ret != 2) {
			fprintf(stderr, "www: Request reading error\n");
			// ...
	}
	
	if (strcmp(filename, "/") != 0) {
		fclose(f);
		return;
	}
	
	setvbuf(f, NULL, _IOFBF, BUFSIZ); /* Reenable buffering */
	fprintf(f, "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n\r\n");
	char *index = "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">\n<html lang=\"en\">\n<head>\n  <meta charset=\"UTF-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n  <style>\n    .table-container {\n      display: block;\n      margin-bottom: 20px;\n    }\n  </style>\n  <title>PSR Sem</title>\n</head>\n\n\n  <body onload=\"setTimeout(function(){location.reload()}, 96);\">\n    <script>document.write(\"<h1 align=\\\"center\\\">PSR motor web server</h1>\\n\\n<!-- First Table -->\\n<div class=\\\"table-container\\\">\\n<table border=\\\"1\\\", width=\\\"400px\\\">\\n    <tr>\\n    <td>Current time</td>\\n    <td>\" + Date.now() + \"</td>\\n    </tr>\\n</table>\\n</div>\\n\\n<div class=\\\"table-container\\\" align=\\\"center\\\">\\n    <svg width=\\\"500\\\" height=\\\"300\\\" xmlns='http://www.w3.org/2000/svg'>\\n        \\n        <g transform=\\\"translate(50,130) scale(1)\\\">\\n          \\n          <!-- Now Draw the main X and Y axis -->\\n          <g style=\\\"stroke-width:2; stroke:black\\\">\\n            <!-- X Axis -->\\n            <path d=\\\"M 0 0 L 400 0 Z\\\"/>\\n            <!-- Y Axis -->\\n            <path d=\\\"M 0 -100 L 0 100 Z\\\"/>\\n          </g>\\n          <g style=\\\"fill:none; stroke:#B0B0B0; stroke-width:1; stroke-dasharray:2 4;text-anchor:end; font-size:30\\\">\\n\\n            <!-- Plot Title -->\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"200\\\" y=\\\"-110\\\" text-anchor=\\\"middle\\\">Position</text>\\n\\n            <!-- Legend -->>\\n            <text style=\\\"fill:red; stroke:none; font-size:13\\\" x=\\\"350\\\" y=\\\"-105\\\" text-anchor=\\\"middle\\\">Desired</text>\\n            <text style=\\\"fill:green; stroke:none; font-size:13\\\" x=\\\"350\\\" y=\\\"-118\\\" text-anchor=\\\"middle\\\">Actual</text>\\n\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"-1\\\" y=\\\"100\\\" >-360</text>\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"-1\\\" y=\\\"0\\\" >0</text>\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"-1\\\" y=\\\"-100\\\" >360</text>\\n            <g style=\\\"text-anchor:middle\\\">\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"100\\\" y=\\\"20\\\" >100</text>\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"200\\\" y=\\\"20\\\" >200</text>\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"300\\\" y=\\\"20\\\" >300</text>\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"400\\\" y=\\\"20\\\" >400</text>\\n            </g>\\n          </g>\\n          <polyline\\n                points=\\\"";
	int idx = 0;
	fprintf(f, "%s", index);
	// Loop through the array and concatenate index and value with commas
	for (size_t i = first; i != last; i++) {
		if (i == WWW_ARRAY_SIZE) {
			i = 0;
		}
		
		fprintf(f, "%d,%d \0", idx, -1*desired_arr[i]/5);
		// Add a space after each element (except the last one)
		idx++;
	}
	
	fprintf(f,	"\\\"\\n                style=\\\"stroke:red; stroke-width: 1; fill : none;\\\"/>\\n            \\n                <polyline\\n                points=\\\" ");
	idx = 0;
	// Loop through the array and concatenate index and value with commas
	for (size_t i = first; i != last; i++) {
		if (i == WWW_ARRAY_SIZE) {
			i = 0;
		}
		
		fprintf(f, "%d,%d \0", idx, -1*actual_arr[i]/5);
		// Add a space after each element (except the last one)
		idx++;
	}
	
	
	fprintf(f, "\\\"\\n                style=\\\"stroke:green; stroke-width: 1; fill : none;\\\"/>\\n\\n        \\n        </g>\\n      </svg>\\n</div><div class=\\\"table-container\\\" align=\\\"center\\\">\\n    <svg width=\\\"500\\\" height=\\\"300\\\" xmlns='http://www.w3.org/2000/svg'>\\n        \\n        <g transform=\\\"translate(50,130) scale(1)\\\">\\n          \\n          <!-- Now Draw the main X and Y axis -->\\n          <g style=\\\"stroke-width:2; stroke:black\\\">\\n            <!-- X Axis -->\\n            <path d=\\\"M 0 0 L 400 0 Z\\\"/>\\n            <!-- Y Axis -->\\n            <path d=\\\"M 0 -100 L 0 100 Z\\\"/>\\n          </g>\\n          <g style=\\\"fill:none; stroke:#B0B0B0; stroke-width:1; stroke-dasharray:2 4;text-anchor:end; font-size:30\\\">\\n\\n            <!-- Plot Title -->\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"200\\\" y=\\\"-110\\\" text-anchor=\\\"middle\\\">PWM</text>\\n\\n            <!-- Legend -->>\\n            <text style=\\\"fill:blue; stroke:none; font-size:13\\\" x=\\\"350\\\" y=\\\"-105\\\" text-anchor=\\\"middle\\\">Duty</text>\\n\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"-1\\\" y=\\\"100\\\" >-100</text>\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"-1\\\" y=\\\"0\\\" >0</text>\\n            <text style=\\\"fill:black; stroke:none\\\" x=\\\"-1\\\" y=\\\"-100\\\" >100</text>\\n            <g style=\\\"text-anchor:middle\\\">\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"100\\\" y=\\\"20\\\" >100</text>\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"200\\\" y=\\\"20\\\" >200</text>\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"300\\\" y=\\\"20\\\" >300</text>\\n          <text style=\\\"fill:black; stroke:none\\\" x=\\\"400\\\" y=\\\"20\\\" >400</text>\\n            </g>\\n          </g>\\n          <polyline\\n                points=\\\" ");
	
	
	idx = 0;
	// Loop through the array and concatenate index and value with commas
	for (size_t i = first; i != last; i++) {
		if (i == WWW_ARRAY_SIZE) {
			i = 0;
		}
		
		fprintf(f, "%d,%d \0", idx, -1*pwm_arr[i]/5);
		// Add a space after each element (except the last one)
		idx++;
	}
	
	
	fprintf(f, "\\\"\\n                style=\\\"stroke:blue; stroke-width: 1; fill : none;\\\"/>\\n\\n        \\n        </g>\\n      </svg>\\n</div>\");</script>\n  </body>\n</html>");
	
	fflush(f);
	fclose(f);

}

void www()
{
    int s;
    int newFd;
    struct sockaddr_in serverAddr;
    struct sockaddr_in clientAddr;
    socklen_t sockAddrSize;

    sockAddrSize = sizeof(struct sockaddr_in);
    bzero((char *)&serverAddr, sizeof(struct sockaddr_in));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
        printf("Error: www: socket(%d)\n", s);
        return;
    }

    if (bind(s, (struct sockaddr *)&serverAddr, sockAddrSize) == ERROR) {
        printf("Error: www: bind\n");
        return;
    }

    if (listen(s, SERVER_MAX_CONNECTIONS) == ERROR) {
        perror("www listen");
        close(s);
        return;
    }

    printf("www server running\n");
    
    int req_id = 0;

    while (1) {
        /* accept waits for somebody to connect and the returns a new file descriptor */
        if ((newFd = accept(s, (struct sockaddr *)&clientAddr, &sockAddrSize)) == ERROR) {
            perror("www accept");
            close(s);
            return;
        }

        /* The client connected from IP address inet_ntoa(clientAddr.sin_addr)
           and port ntohs(clientAddr.sin_port).

           Start a new task for each request. The task will parse the request
           and sends back the response.

           Don't forget to close newFd at the end */
        char name[20];
        sprintf(name, "tServer%d", req_id % SERVER_MAX_CONNECTIONS);
        TASK_ID s = taskSpawn(name, 130, 0, 50000, (FUNCPTR) server_task, newFd, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        req_id ++;
    }
}
